"""
Класс School.

Поля:
список людей в школе (общий list для Pupil и Worker): people,
номер школы: number.

Методы:
get_avg_mark: вернуть средний балл всех учеников школы
get_avg_salary: вернуть среднюю зп работников школы
get_worker_count: вернуть сколько всего работников в школе
get_pupil_count: вернуть сколько всего учеников в школе
get_pupil_names: вернуть все имена учеников (с повторами, если есть)
get_unique_worker_positions: вернуть список всех должностей работников (без повторов, подсказка: set)
get_max_pupil_age: вернуть возраст самого старшего ученика
get_min_worker_salary: вернуть самую маленькую зп работника
get_min_salary_worker_names: вернуть список имен работников с самой маленькой зп
(список из одного или нескольких элементов)
"""
from topic_07_oop.hw.class_4_1_pupil import Pupil
from topic_07_oop.hw.class_4_2_worker import Worker

class School:

    def __init__(self, people, worker):
        self.people = people
        self.worker = worker

    def get_avg_mark(self):
        all_marks = [mark.get_avg_mark() for mark in self.people if isinstance(mark, Pupil)]
        return sum(all_marks)/len(all_marks)

    def get_avg_salary(self):
        all_salary = [money.salary for money in self.people if isinstance(money, Worker)]
        return sum(all_salary)/len(all_salary)

    def get_worker_count(self):
        return len([worker for worker in self.people if isinstance(worker, Worker)])

    def get_pupil_count(self):
        return len([pupil for pupil in self.people if isinstance(pupil, Pupil)])

    def get_pupil_names(self):
        return [scholar.name for scholar in self.people if isinstance(scholar, Pupil)]

    def get_unique_worker_positions(self):
        return set([w.position for w in self.people if isinstance(w, Worker)])

    def get_max_pupil_age(self):
        return max([pa.age for pa in self.people if isinstance(pa, Pupil)])

    def get_min_worker_salary(self):
        return min([ws.salary for ws in self.people if isinstance(ws, Worker)])

    def get_min_salary_worker_names(self):
        return [msw.name for msw in self.people if isinstance(msw, Worker) if msw.salary == self.get_min_worker_salary()]

if __name__ == '__main__':
    kimi = Pupil('Kimi-Matias Räikkönen', 11, {'math': [5, 5, 4], '+english': [3, 4, 3], 'history': [3, 4, 5]})
    hill = Pupil('Daimon Hill', 12, {'math': [4, 4, 4], 'english': [5, 5, 5], 'history': [5, 4, 5]})
    todt = Worker('Жан Тодт', 1000000, 'Главный среди главных')
    brawn = Worker('Росс Браун', 750000, 'Тоже главный но есть главнее')

    formula_1_school = School([kimi, hill, todt, brawn], 1)

    print(formula_1_school.get_avg_mark())
    print(formula_1_school.get_avg_salary())
    print(formula_1_school.get_pupil_names())
    print(formula_1_school.get_min_worker_salary())
    print(formula_1_school.get_unique_worker_positions())
    print(formula_1_school.get_max_pupil_age())
    print(formula_1_school.get_min_salary_worker_names())
