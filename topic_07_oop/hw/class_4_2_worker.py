"""
Класс Worker.

Поля:
имя: name,
зарплата: salary,
должность: position.

Методы:
__gt__: возвращает результат сравнения (>) зарплат работников.
__len__: возвращает количетсво букв в названии должности.
"""

class Worker:
    def __init__(self, name, salary, position):
        self.name = name
        self.salary = salary
        self.position = position

    def __str__(self):
        return f'{self.name.title()} работает в нашей школе'

    def __gt__(self, other):
        return self.salary > other.salary

    def __len__(self):
        return len(self.position)

if __name__ =='__main__':
    todt = Worker('Жан Тодт', 1000000, 'Главный среди главных')
    brawn = Worker('Росс Браун', 750000, 'Тоже главный но есть главнее')

    print(todt)
    print(brawn)
    print(todt.__gt__(brawn))
    print(brawn.__len__())





