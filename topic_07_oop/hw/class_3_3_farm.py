"""
Класс Farm.

Поля:
животные (list из произвольного количества Goat и Chicken): zoo_animals
(вначале список пуст, потом добавялем животных методами append и extend самостоятельно),
наименование фермы: name,
имя владельца фермы: owner.

Методы:
get_goat_count: вернуть количество коз на ферме (*Подсказка isinstance или type == Goat),
get_chicken_count: вернуть количество куриц на ферме,
get_animals_count: вернуть количество животных на ферме,
get_milk_count: вернуть сколько молока можно получить в день,
get_eggs_count: вернуть сколько яиц можно получить в день.
"""

from topic_07_oop.hw.class_3_1_chicken import Chicken
from topic_07_oop.hw.class_3_2_goat import Goat

class Farm:

    def __init__(self, name, owner):
        self.zoo_animals = []
        self.name = name
        self.owner = owner

    def __str__(self):
        return f'Ферма порядочности'

    def get_goat_count(self):
        return len([name for name in self.zoo_animals if isinstance(name, Goat)])

    def get_chicken_count(self):
        return len([name for name in self.zoo_animals if isinstance(name, Chicken)])

    def get_animals_count(self):
        return len(self.zoo_animals)

    def get_milk_count(self):
        return sum([milk.milk_per_day for milk in self.zoo_animals if isinstance(milk, Goat) ])

    def get_eggs_count(self):
        return sum([eggs.eggs_per_day for eggs in self.zoo_animals if isinstance(eggs, Chicken)])

if __name__ == '__main__':
    south_farm = Farm('Tegridy Farms', 'Рэнди Марш')
    south_farm.zoo_animals.extend([Chicken('Vlad', 21, 1),
                                   Chicken('Alex', 26, 0),
                                   Goat('Милка', 3, 5),
                                   Goat('Карл', 1, 2),
                                   Goat('Маркс', 0, 0)])

    print(south_farm)
    print(south_farm.get_goat_count())
    print(south_farm.get_chicken_count())
    print(south_farm.get_animals_count())
    print(south_farm.get_milk_count())
    print(south_farm.get_eggs_count())