"""
Класс Pupil.

Поля:
имя: name,
возраст: age,
dict с оценками: marks (пример: {'math': [3, 5], 'english': [5, 5, 4] ...].

Методы:
get_all_marks: получить список всех оценок,
get_avg_mark_by_subject: получить средний балл по предмету (если предмета не существует, то вернуть 0),
get_avg_mark: получить средний балл (все предметы),
__le__: вернуть результат сравнения (<=) средних баллов (все предметы) двух учеников.
"""

class Pupil:
    def __init__(self, name, age, marks):
        self.name = name
        self.age = age
        self.marks = marks

    def get_all_marks(self):
        all_marks = []
        for mark in list(self.marks.values()):
            all_marks.extend(mark)
        return all_marks

    def get_avg_mark_by_subject(self, subject):
        if subject in self.marks.keys():
            subject_marks = self.marks.get(subject, ())
            return sum(subject_marks)/len(subject_marks)
        else:
            return 0

    def get_avg_mark(self):
        all_marks = []
        for mark in list(self.marks.values()):
            all_marks.extend(mark)
        return sum(all_marks)/len(all_marks)

    def __le__(self, other):
        return self.get_avg_mark() <= other.get_avg_mark()

    def __str__(self):
        return f'Welcome {self.name}'

if __name__ == '__main__':
    kimi = Pupil('Kimi-Matias Räikkönen', 11, {'math': [5, 5, 4], 'english': [3, 4, 3], 'history': [3, 4, 5]})
    hill = Pupil('Daimon Hill', 12, {'math': [4, 4, 4], 'english': [5, 5, 5], 'history': [5, 4, 5]})

    print(kimi)
    print(hill)
