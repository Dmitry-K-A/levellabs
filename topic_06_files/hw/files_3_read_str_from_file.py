"""
Функция read_str_from_file.

Принимает 1 аргумент: строка (название файла или полный путь к файлу).

Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
"""

def read_str_from_file(path):
    with open(path, 'r') as f:
        for line in f:
            print(line.strip())

if __name__ == '__main__':
    filename = 'check_save_dict_to_file_classic.txt'
    read_str_from_file(filename)