"""
Функция save_dict_to_file_pickle.

Принимает 2 аргумента:
    строка (название файла или полный путь к файлу)
    словарь (для сохранения).

Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
"""

import pickle


def save_dict_to_file_pickle(path, dictionary):
    with open(path, 'wb') as pickle_txt_file:
        pickle.dump(dictionary, pickle_txt_file)


if __name__ == '__main__':
    filename = 'check_save_dict_to_file_pickle.pkl'
    dict1 = {1: 'one', 2: 'two', 3: 'three'}

    save_dict_to_file_pickle(filename, dict1)
