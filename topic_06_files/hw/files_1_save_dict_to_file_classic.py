"""
Функция save_dict_to_file_classic.

Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

Сохраняет список в файл. Проверить, что записалось в файл.
"""


def save_dict_to_file_classic(path, dictionary):
    with open(path, 'w') as my_file:
        my_file.write(str(dictionary))


if __name__ == '__main__':

    filename = 'check_save_dict_to_file_classic.txt'
    dict1 = {1: 'one', 2: 'two', 3: 'three'}
    save_dict_to_file_classic(filename, dict1)
