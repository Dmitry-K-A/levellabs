"""
lambda перемножает аргументы a, b и c и выводит результат
"""

bar = lambda a, b, c: a * b * c

print(bar(1, 2, 3))
print(bar(-1, -2, -3))
print(bar(9, 7, 4))
