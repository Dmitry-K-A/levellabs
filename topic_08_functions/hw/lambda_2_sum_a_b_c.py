"""
lambda суммирует аргументы a, b и c и выводит результат
"""


foo = lambda a, b, c: sum((a, b, c))

print(foo(1, 2, 3))
print(foo(1, 5, 3))
print(foo(1, -3.4, 3))

