"""
lambda делит аргумент a на аргумент b и выводит результат
"""

print((lambda a, b: a / b)(9, 2))
print(int((lambda a, b: a / b)(9, 2)))
print('LambaLambada ' + str((lambda a, b: a / b)(9, 2)))