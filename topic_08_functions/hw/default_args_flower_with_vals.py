"""
Функция flower_with_default_vals.

Принимает 3 аргумента:
цветок (по умолчанию "ромашка"),
цвет (по умолчанию "белый"),
цена (по умолчанию 10.25).

Функция flower_with_default_vals выводит строку в формате
"Цветок: <цветок> | Цвет: <цвет> | Цена: <цена>".

При этом в функции flower_with_default_vals есть проверка на то, что цветок и цвет - это строки

(* Подсказка: type(x) == str или isinstance(s, str)), а также цена - это число больше 0, но меньше 10000.
В функции main вызвать функцию flower_with_default_vals различными способами
(перебрать варианты: цветок, цвет, цена, цветок цвет, цветок цена, цвет цена, цветок цвет цена).

(* Использовать именованные аргументы).
"""


def flower_with_default_vals(flower='ромашка', color="белый", price=10.25):

    if price < 0 or price > 10000:
        print("Incorrect price value")

    if type(flower) != str or type(color) != str:
        print('String error')

    print(f'Цветок: {flower} | Цвет: {color} | Цена: {price} рублей')


if __name__ == '__main__':
    flower_with_default_vals(flower="Туфелька Божьей Матери")
    flower_with_default_vals(color='Красновато-розовый окрас')
    flower_with_default_vals(price=279)
    flower_with_default_vals(flower="Cердце Жаннеты", color='Красновато-розовый окрас')
    flower_with_default_vals(flower="Цветок сердца", price=279)
    flower_with_default_vals(color='Красновато-розовый окрас', price=279)
    flower_with_default_vals(flower="Дама в ванне", color='Красновато-розовый окрас', price=279)


