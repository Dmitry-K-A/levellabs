"""
Функция len_count_first.

Принимает 2 аргумента: кортеж my_tuple и строку word.

Возвращает кортеж состоящий из
длины кортежа,
количества word в кортеже my_tuple,
первого элемента кортежа.

Пример:
my_tuple=('55', 'aa', '66')
word = '66'
результат (3, 1, '55').

Если вместо tuple передано что-то другое, то возвращать строку 'First arg must be tuple!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если tuple пуст, то возвращать строку 'Empty tuple!'.
"""


def len_count_first(my_tuple, word):

    if type(my_tuple) != tuple:
        return f'First arg must be tuple!'

    if type(word) != str:
        return f'Second arg must be str!'

    if my_tuple == ():
        return f'Empty tuple!'

    result = (len(my_tuple), my_tuple.count(word), my_tuple[0])

    return result

if __name__ == "__main__":

    # user_tuple = tuple(x for x in input("Введите кортеж, разделитель пробел: ").split(' '))
    user_tuple = tuple(input("Введите кортеж: "))
    user_word = input("Укажите нужное слово: ")

    print(len_count_first(user_tuple, user_word))
