"""
Функция list_to_dict.

Принимает 2 аргумента:
    * список
    * значение для поиска.

Возвращает словарь, в котором:
    * ключ = значение из списка
    * значение (списком):
        ** индекс (последний, если есть несколько одинаковых значений) этого элемента в списке
        ** равно ли значение из списка искомому значению (True | False)
        ** количество элементов в словаре

Если список пуст, то возвращать пустой словарь.
Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.

ВНИМАНИЕ: количество элементов в словаре не всегда равно количеству элементов в списке!
ВНИМАНИЕ: при повторяющихся ключах в dict записывается значение последнего добавленного ключа.
ВНИМАНИЕ: нумерация индексов начинается с 0!
"""
def list_to_dict2(input_list, search_word):



def list_to_dict(input_list, search_word):

    empty_dict = {}

    if type(input_list) != list:
        return f'First arg must be list!'
    if len(input_list) == 0:
        return empty_dict

    unique_elements = len(set(input_list))

    for element in input_list:
        # Чтобы получить последний индекс элемента необходимо из длины списка вычесть:
        # * единицу - потому что нумерация начинается с 0
        # * индекс искомого элемента в перевернутой копии списка - нотация среза [::-1]
        last_index = len(input_list) - 1 - input_list[::-1].index(element)
        empty_dict[element] = (last_index, bool(element == search_word), unique_elements)

    not_so_empty_dict = empty_dict

    return not_so_empty_dict


if __name__ == "__main__":
    # list_case_1 = input("Enter: ")
    list_case_1 = ['a', 'bc', None, 'df']
    # search_case_1 = input("Enter: ")
    search_case_1 = None
    print(list_to_dict(list_case_1, search_case_1))
    # {None: (2, True, 4),
    #  'a': (0, False, 4),
    #  'bc': (1, False, 4),
    #  'df': (3, False, 4)}),

    # list_case_2 = input("Enter: ")
    list_case_2 = [0, 1, 2, 3]
    # search_case_2 = input("Enter: ")
    search_case_2 = 'e'
    print(list_to_dict(list_case_2, search_case_2))
    # {0: (0, False, 4),
    # 1: (1, False, 4),
    # 2: (2, False, 4),
    # 3: (3, False, 4)}),