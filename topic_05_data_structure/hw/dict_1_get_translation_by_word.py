"""
Функция get_translation_by_word.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (ru).

Возвращает все варианты переводов (list), если такое слово есть в словаре,
если нет, то "Can't find Russian word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_translation_by_word(ru_eng, ru):

    if type(ru_eng) != dict:
        return f'Dictionary must be dict!'
    if type(ru) != str:
        return f'Word must be str!'

    if ru_eng == {}:
        return f'Dictionary is empty!'
    if ru == '':
        return f'Word is empty!'

    if ru not in ru_eng.keys():
        return f'Can\'t find Russian word: {ru}'
    else:
        return ru_eng[ru]


if __name__ == "__main__":

    user_ru_eng = tuple(input("Введите словарь: "))
    user_ru = input("Укажите нужное слово: ")

    print(get_translation_by_word(user_ru_eng, user_ru))
