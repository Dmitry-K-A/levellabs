"""
Функция pow_odd.

Принимает число n.

Возвращает словарь, в котором
ключ - это нечетное число в диапазоне от 0 до n (включая)
значение - квадрат ключа.

Пример: n = 5, четные числа [1, 3, 5], результат {1: 1, 3: 9, 5: 25}.

Если n не является int, то вернуть строку 'Must be int!'.
"""


def pow_odd(n=int):
    if type(n) != int:
        return 'Must be int!'

    result = {num: (num * num) for num in range(1, n + 1, 2)}

    return result


if __name__ == "__main__":
    user_n = input("Введите число: ")
    print(pow_odd(user_n))
