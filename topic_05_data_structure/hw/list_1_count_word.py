"""
Функция count_word.

Принимает 2 аргумента:
список слов my_list и
строку word.

Возвращает количество word в списке my_list.

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо строки передано что-то другое, то возвращать строку 'Second arg must be str!'.
Если список пуст, то возвращать строку 'Empty list!'.
"""


def count_word(my_list, word):

    if type(my_list) != list:
        return f'First arg must be list!'
    if type(word) != str:
        return f'Second arg must be str!'
    if len(my_list) == 0:
        return f'Empty list!'


    return my_list.count(word)


if __name__ == "__main__":
    user_list = input("Введите список запросов: ")
    user_word = input("Введите искомое слово: ")

    print(count_word(user_list, user_word))


