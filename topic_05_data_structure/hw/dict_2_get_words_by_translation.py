"""
Функция get_words_by_translation.

Принимает 2 аргумента:
ru-eng словарь содержащий {ru_word: [eng_1, eng_2 ...], ...}
слово для поиска в словаре (eng).

Возвращает список всех вариантов переводов (ru), если такое слово есть в словаре (eng),
если нет, то "Can't find English word: {word}".

Если вместо словаря передано что-то другое, то возвращать строку "Dictionary must be dict!".
Если вместо строки для поиска передано что-то другое, то возвращать строку "Word must be str!".

Если словарь пустой, то возвращать строку "Dictionary is empty!".
Если строка для поиска пустая, то возвращать строку "Word is empty!".
"""


def get_words_by_translation(ru_eng, search_eng):

    if type(ru_eng) != dict:
        return f'Dictionary must be dict!'
    if type(search_eng) != str:
        return f'Word must be str!'

    if ru_eng == {}:
        return f'Dictionary is empty!'
    if search_eng == '':
        return f'Word is empty!'

    result = []

    for key, value in ru_eng.items():
        if search_eng in value:
            result.append(key)

    return result if len(result) > 0 else f'Can\'t find English word: {search_eng}'


if __name__ == "__main__":

    user_ru_eng = input("Введите словарь: ")
    user_eng = input("Укажите нужное слово: ")

    print(get_words_by_translation(user_ru_eng, user_eng))
