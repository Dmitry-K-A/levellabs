"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список:
* список ключей,
* список значений,
* количество уникальных элементов в списке ключей,
* количество уникальных элементов в списке значений

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.
Если dict пуст, то возвращать ([], [], 0, 0).
"""


def dict_to_list(mutagen):

    if type(mutagen) != dict:
        return f'Must be dict!'

    empty_dict = ([], [], 0, 0)

    if len(mutagen) == 0:
        return empty_dict

    key_list = []
    value_list = []

    for key, val in mutagen.items():
        key_list.append(key)
        value_list.append(val)

    key_unique = len(set(key_list))
    value_unique = len(set(value_list))

    result = (key_list, value_list, key_unique, value_unique)

    return result

if __name__ == "__main__":

    # use_case_1 = input("Введите словарь: ")
    use_case_1 = {1: 'q'}
    # ([1], ['q'], 1, 1)

    print(dict_to_list(use_case_1))

    # use_case_2 = input("Введите словарь: ")
    use_case_2 = {1: 'q', 2: 'w', 3: '3'}
    # ([1, 2, 3], ['q', 'w', '3'], 3, 3)

    print(dict_to_list(use_case_2))
