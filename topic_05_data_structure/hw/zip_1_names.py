"""
Функция zip_names.

Принимает 2 аргумента: список с именами и множество с фамилиями.

Возвращает список с парами значений из каждого аргумента.

Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
Если вместо set передано что-то другое, то возвращать строку 'Second arg must be set!'.

Если list пуст, то возвращать строку 'Empty list!'.
Если set пуст, то возвращать строку 'Empty set!'.

Если list и set различного размера, обрезаем до минимального (стандартный zip).
"""

# from itertools import zip_longest


def zip_names(names, last_names):

    if type(names) != list:
        return f'First arg must be list!'
    if type(last_names) != set:
        return f'Second arg must be set!'
    if len(names) == 0:
        return f'Empty list!'
    if len(last_names) == 0:
        return f'Empty set!'

    return list(zip(names, last_names))


if __name__ == "__main__":
    # user_tuple = tuple(x for x in input("Введите кортеж, разделитель пробел: ").split(' '))
    user_names = input("Введите список имен: ")
    user_last_names = input("Введите множество фамилий: ")

    print(zip_names(user_names, user_last_names))
