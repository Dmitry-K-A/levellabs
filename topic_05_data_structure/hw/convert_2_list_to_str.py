"""
Функция list_to_str.

Принимает 2 аргумента:
* список
* разделитель (строка).

Возвращает:
* строку полученную разделением элементов списка разделителем
* количество разделителей в получившейся строке в квадрате

Если вместо списка передано что-то другое, то возвращать строку 'First arg must be list!'.
Если разделитель не является строкой, то возвращать строку 'Second arg must be str!'.
Если список пуст, то возвращать пустой tuple().

ВНИМАНИЕ: в списке могут быть элементы любого типа (нужно конвертировать в строку).
"""


def list_to_str(input_list, input_string):

    if type(input_list) != list:
        return f'First arg must be list!'
    if type(input_string) != str:
        return f'Second arg must be str!'

    if len(input_list) == 0:
        return tuple()

    list_length = len(input_list)

    elements_conversion = []

    while list_length > 0:
        for element in input_list:
            elements_conversion.append(str(element))
            list_length -= 1

    converted_string = input_string.join(elements_conversion)

    return converted_string, (len(input_list)-1) * (len(input_list)-1)


if __name__ == "__main__":

    user_list_case_1 = [1, 2, 3, 4]
    user_string_case_1 = '!'
    print(list_to_str(user_list_case_1, user_string_case_1))

    user_list_case_2 = [1, '2', 'awe', [1, 2, 3]]
    user_string_case_2 = '??'
    print(list_to_str(user_list_case_2, user_string_case_2))
