"""
Функция pow_odd.

Принимает число n.

Возвращает список, состоящий из квадратов нечетных чисел в диапазоне от 0 до n (не включая).

Пример: n = 7, нечетные числа [1, 3, 5], результат [1, 9, 25]

Если n не является int, то вернуть строку 'Must be int!'.
"""


def pow_odd(n):
    if type(n) != int:
        return 'Must be int!'

    #result = []

    #for num in range(1, n, 2):
    #    result.append(int(num * num))

    return [(x * x) for x in range(1, n, 2)]


if __name__ == "__main__":
    user_n = input("Введите число: ")
    print(pow_odd(user_n))
