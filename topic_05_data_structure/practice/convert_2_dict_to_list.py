"""
Функция dict_to_list.

Принимает 1 аргумент: словарь.

Возвращает список: [
    список (list) ключей,
    список (list) значений,
    количество элементов в списке ключей в степени 3,
    количество уникальных элементов в списке значений,
    хотя бы один ключ равен одному из значений (True | False).
]

Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

Если dict пуст, то возвращать [[], [], 0, 0, False].
"""


def dict_to_list(my_dict: dict):
    if type(my_dict) != dict:
        return 'Must be dict!'
    elif not my_dict:
        return '[[], [], 0, 0, False]'

    key_list = list(my_dict.keys())
    values_list = list(my_dict.values())
    len_keys_pow_3 = len(key_list) ** 3
    len_unique_values = len(set(values_list))
    key_equal_value = False

    for k, v in my_dict.items():
        # for v in my_dict.values():
        if k == v:
            key_equal_value = True
            break

    return [key_list, values_list, len_keys_pow_3, len_unique_values, key_equal_value]

if __name__ == "__main__":
    x = input()
    print(dict_to_list(x))