"""
Функция print_nth_symbols.

Принимает строку и натуральное число n (целое число > 0).
Вывести символы с индексом n, n*2, n*3 и так далее.
Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'

Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
Если тип n не int, то вывести строку 'Must be int!'.

Если n больше длины строки, то вывести пустую строку.
"""


def print_nth_symbols(user_str, digit):
    str_l = len(user_str)
    if type(digit) != int:
        print('Must be int!')
    elif digit <= 0:
        print('Must be > 0!')
    elif str_l < digit:
        print("")
    else:
        print(user_str[digit: str_l + 1: digit])

if __name__ == "__main__":
    enter_str = input("Введите первую строку: ")
    enter_n = input("Введите первую строку: ")

    print_nth_symbols(enter_str, enter_n)