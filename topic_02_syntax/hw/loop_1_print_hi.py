"""
Функция print_hi.

Принимает число n.
Выведите на экран n раз фразу "Hi, friend!"
Если число <= 0, тогда вывести пустую строку.
Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"

!сделать все сразу в return с помощью тернарного оператора (для тренировки его использования)
"""



def print_hi(n):
    print("Hi, friend!" * n) if n > 0 else print("")


if __name__ == "__main__":
    digit = int(input("Здравствуй, друг! Сколько вас сегодня? "))
    print_hi(digit)