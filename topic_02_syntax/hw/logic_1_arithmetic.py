"""
Функция arithmetic.

Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
Если третий аргумент +, сложить их;
если —, то вычесть;
если *, то умножить;
если /, то разделить (первое на второе).
В остальных случаях вернуть строку "Unknown operator".
Вернуть результат операции.

"""


def arithmetic(number_1, number_2, the_operator):
    if the_operator == '+':
        return number_1 + number_2
    elif the_operator == '-':
        return number_1 - number_2
    elif the_operator == '*':
        return number_1 * number_2
    elif the_operator == '/':
        return number_1 / number_2
    else:
        return "Unknown operator"


if __name__ == "__main__":
    number_one = int(input("Введите первое число: "))
    number_two = int(input("Введите второе число: "))
    operator = input("Укажите желаемую операцию: +, -, * или /. ")

    print(arithmetic(number_one, number_two, operator))


"""
def arithmetic(number_1, number_2, op):
    operators = {
        '+': operator.add(number_1, number_2),
        '-': operator.sub(number_1, number_2),
        '*': operator.mul(number_1, number_2),
        '/': operator.truediv1(number_1, number_2)
    }
    if op not in operators.keys():
        return "Unknown operator"
"""

