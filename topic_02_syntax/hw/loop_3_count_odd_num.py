"""
Функция count_odd_num.

Принимает натуральное число (целое число > 0).
Верните количество нечетных цифр в этом числе.
Если число меньше или равно 0, то вернуть "Must be > 0!".
Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".
"""


def count_odd_num(digit):



    if type(digit) != int:
        return "Must be int!"

    if digit <= 0:
        return "Must be > 0!"

    odds = 0

    # https://www.kite.com/python/answers/how-to-convert-an-integer-to-a-list-in-python
    # https://www.geeksforgeeks.org/python-convert-number-to-list-of-integers/

    digit_list = [int(x) for x in str(digit)]

    for i in digit_list:
        if i % 2 != 0:
            odds += 1

    return odds


if __name__ == "__main__":
    number = input("Введите число, в котором нужно посчитать количество нечетных цифр: ")
    print(count_odd_num(number))
