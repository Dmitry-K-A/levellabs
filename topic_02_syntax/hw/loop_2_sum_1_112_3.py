"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def sum_1_112_3():
    final_sum = 0

    for i in range(1, 113, 3):
        final_sum += i

    return final_sum


if __name__ == "__main__":
    print(sum_1_112_3())