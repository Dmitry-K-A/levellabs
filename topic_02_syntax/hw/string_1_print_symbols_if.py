"""
Функция print_symbols_if.
Принимает строку.
Если строка нулевой длины, то вывести строку "Empty string!".
Если длина строки больше 5, то вывести первые три символа и последние три символа.
Пример: string='123456789' => result='123789'
Иначе вывести первый символ столько раз, какова длина строки.
Пример: string='345' => result='333'
в print_symbols_if правильнее было бы использовать переменную для хранения длины строки (len(user_string)), то есть один раз вычислить длину и записать ее, а дальше использовать это значение
"""


def print_symbols_if(phrase):
    user_string = len(phrase)

    if user_string > 5:
        print(phrase[0:3] + phrase[-3:-1] + phrase[-1])
    elif user_string == 0:
        print("Empty string!")
    else:
        print(str(phrase[0]) * user_string)


if __name__ == "__main__":
    string_input = input("Введите строку: ")
    print(print_symbols_if(string_input))
