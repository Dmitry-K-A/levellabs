"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def check_sum(a, b, c):
    if a + b == c or b + c == a or a + c == b:
        return True
    else:
        return False


if __name__ == "__main__":
    x, y, z = int(input("Введите первое число: ")), int(input("Введите второе число: ")), int(
        input("Введите третье число: "))

    print(check_sum(x, y, z))
