"""
Функция check_substr.

Принимает две строки.
Если меньшая по длине строка содержится в большей, то возвращает True,
иначе False.
Если строки равны, то False.
Если одна из строк пустая, то True.
"""


def check_substr(string1, string2):
    if len(string1) == len(string2):
        return False
    elif string1 in string2 or string2 in string1:
        return True
    else:
        return False


if __name__ == "__main__":
    string_1 = input("Введите первую строку: ")
    string_2 = input("Введите первую строку: ")

    check_substr(string_1, string_2)
