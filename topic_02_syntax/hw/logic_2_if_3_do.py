"""
Функция if_3_do.
Принимает число.
Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
Вернуть результат.
"""


def if_3_do(number):
    return number + 10 if number > 3 else number - 10


if __name__ == "__main__":
    number = int(input("\nВведите число: "))
    print(if_3_do(number))
